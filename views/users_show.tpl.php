<?php
if ($_SESSION["current_user"]->is_admin()) {
  echo "<a href='". URL_BASE. "/controllers/user_add.php'>";
  echo "<button>Add new user</button>";
  echo "</a>";
  echo "<br/>";
}

foreach ($users as $user) {
  $user_params["user"] = $user;
  $contents[] = View::fetch(realpath("../views/user_show.tpl.php"), $user_params);
}
echo implode("<br/>", $contents);
?>