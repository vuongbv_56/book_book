
<?php
/* require $user parameter; */
$info = $user->get_information();

echo "<a href='". URL_BASE. "/controllers/user_show.php?user_id=". $info["user_id"]. "'>";
echo "User";
echo "<a>";
echo "<br>";

echo "<a href='". URL_BASE. "/controllers/user_edit.php?user_id=". $info["user_id"]. "'>";
echo "Edit";
echo "<a>";
echo " | ";

echo "<a href='". URL_BASE. "/controllers/user_delete.php?user_id=". $info["user_id"]. "'>";
echo "Delete";
echo "<a>";
echo "<br>";

echo "<img src='". URL_BASE. "/controllers/user_avatar.php?user_id=". $info["user_id"]. "'/>";
echo "<br/>";

foreach ($info as $key => $value) {
  if ($key == "avatar_id")
    continue;
  if ($key == "is_admin")
    $value = $info["is_admin"] ? "yes" : "no";
  echo $key. " : ". $value;
  echo "<br/>";
}

?>