<?php
/* require $book parameter; */
$info = $book->get_information();

echo "<a href='". URL_BASE. "/controllers/book_show.php?book_id=". $info["book_id"]. "'>";
echo "Book";
echo "<a>";
echo "<br>";

if (isset($is_liked)) {
  $action = $is_liked? "delete": "add";
  echo "<a href='". URL_BASE. "/controllers/favorite_$action.php?book_id=". $info["book_id"]. "'>";
  echo $is_liked? "Unlike" : "Like";
  echo "<a>";
}

if ($_SESSION["current_user"]->is_admin()) {
  echo " | ";
  echo "<a href='". URL_BASE. "/controllers/book_edit.php?book_id=". $info["book_id"]. "'>";
  echo "Edit";
  echo "<a>";
  echo " | ";

  echo "<a href='". URL_BASE. "/controllers/book_delete.php?book_id=". $info["book_id"]. "'>";
  echo "Delete";
}
  echo "<br/>";
foreach ($info as $key => $value) {
  echo $key. " : ". $value;
  echo "<br/>";
}
echo "<a href='". URL_BASE. "/controllers/book_comments.php?book_id=". $info["book_id"]. "'>";
echo "Comments on this book";
echo "<a>";
echo "<br>";

?>