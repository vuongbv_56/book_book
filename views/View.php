<?php
class View {
  static function fetch($file, $vars) {
    extract($vars);
    
    ob_start();
    require($file);
    return ob_get_clean();    
  }
  static function dump($file, $vars) {
    extract($vars);
    require($file);
  }
}
?>