<?php
$contents = array();
foreach ($books as $book) {
  $book_params["book"] = $book;
  $book_params["is_liked"] = $book->in_favorites_of($_SESSION["current_user"]);
  
  $contents[] = View::fetch(realpath("../views/book_show.tpl.php"), $book_params);
}
echo implode("<br/>", $contents);
?>