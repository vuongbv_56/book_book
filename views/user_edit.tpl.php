<?php
$info = $user->get_information();
?>

<form method="POST" action=<?php echo URL_BASE."/controllers/user_operation.php"; ?> >
   <input type="hidden" name="operation" value=<?php echo $operation?> />
   <input type="hidden" name="user_id"
    value= <?php echo '"'. $info["user_id"]. '"'; ?> />
   Username:
   <input type="text" name="username"
    value= <?php echo '"'. $info["username"]. '"';?> />
   <br />
   Password:
   <input type="text" name="password"
    value = <?php echo '"'. $info["password"]. '"'; ?> />
   <br />
   <?php if ($_SESSION["current_user"]->is_admin()) { ?>
    Is admin?
   
      <input type="checkbox" name="is_admin"
      <?php echo $info["is_admin"] == "1"? " checked ": "";?> >      
   <br />
      <?php } ?>

   <input type="submit" />
</form>

<form action="<?php echo URL_BASE."/controllers/avatar_upload.php";?>" method="post" enctype="multipart/form-data">
    <img src="<?php echo URL_BASE."/controllers/user_avatar.php?user_id=".$info["user_id"];?>" />
    <br/>
    Change avatar by upload another image:
   <input type="hidden" name="user_id"
    value= <?php echo '"'. $info["user_id"]. '"'; ?> />
    <input type="file" name="upload_image">
    <input type="submit" value="Upload">
</form>
