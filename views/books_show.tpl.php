<?php
if ($_SESSION["current_user"]->is_admin()) {
  echo "<a href='". URL_BASE. "/controllers/book_add.php'>";
  echo "<button>Add new book</button>";
  echo "</a>";
  echo "<br/>";
}

foreach ($books as $book) {
  $book_params["book"] = $book;
  $book_params["is_liked"] = $book->in_favorites_of($_SESSION["current_user"]);
  
  $contents[] = View::fetch(realpath("../views/book_show.tpl.php"), $book_params);
}
echo implode("<br/>", $contents);
?>