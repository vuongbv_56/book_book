<?php
require_once(__DIR__."/models/User.php");

session_start();

if (isset($_POST["username"]) and isset($_POST["password"])) {
  require_once(__DIR__."/models/User.php");
  
  $user = new User();
  $user->update_from_array($_POST);
  $fields = array("username", "password");

  $result = $user->retrieve_on_fields($fields);
  if (count($result) > 0) {
    $user = $result[0];
    session_unset();
    $_SESSION["current_user"] = $user;
    
    $info = $user->get_information();
    header("Location: ". URL_BASE. "/controllers/user_show.php?user_id=". $info["user_id"]);
  } else {
    echo "username or password wrong";
  }
  exit();
}
?>

<html>
<body>
<form method="POST" action=<?php echo '"'.$_SERVER["PHP_SELF"].'"';?>>
Login
<br/>
Username:
<input type="text" name="username" />
<br/>
    Password:
    <input type="text" name="password" />
    <br/>
   <input type="submit" />
</form>
    </body>
</html> 