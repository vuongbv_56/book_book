<?php
require_once(__DIR__."/Model.php");
require_once(__DIR__."/Comment.php");
require_once(__DIR__."/Favorite.php");
require_once(__DIR__."/User.php");

class Book extends Model {
  static $table_name = "books";
  static $primary_keys = array("book_id");
  static $field_names = array("book_id", "title", "author");

  function get_comments() {    
    $comment = new Comment();
    $comment->set_field("book_id", $this->get_field("book_id"));
    return $comment->retrieve_on_fields(array("book_id"));
  }

  function in_favorites_of($user) {
    $favor = new Favorite();
    $favor->set_field("user_id", $user->get_field("user_id"));
    $favor->set_field("book_id", $this->get_field("book_id"));
    return count($favor->retrieve_on_fields(array("user_id", "book_id"))) > 0;
  }	    
}

?>