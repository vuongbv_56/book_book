<?php
require_once(__DIR__."/Model.php");

class Avatar extends Model {
  static $table_name = "avatars";
  static $primary_keys = array("user_id");
  static $field_names = array("user_id", "image");
  static $default_avatar_id = 1;

  /* static function filter_danger($field_names, $value) { */
}
?>