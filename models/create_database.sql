DROP DATABASE IF EXISTS book_store;
CREATE DATABASE book_store;

USE book_store;

CREATE TABLE users (
	user_id int unsigned not null auto_increment primary key,
	username char(30) not null,
	password char(20) not null,
	is_admin boolean not null
) engine=innodb ;


CREATE TABLE avatars (
       user_id int unsigned not null auto_increment primary key,
       image blob not null,
       FOREIGN KEY (user_id)
       	       REFERENCES users(user_id)
	       ON DELETE CASCADE
	       ON UPDATE CASCADE
) engine=innodb ;

CREATE TABLE books (
       book_id int unsigned not null auto_increment primary key,
       title char (100) not null,
       author char(30)
) engine=innodb ;

CREATE TABLE comments (
       comment_id int unsigned not null auto_increment primary key,
       book_id int unsigned not null,
       user_id int unsigned not null,
       content text,
       
       FOREIGN KEY (book_id)
       	       REFERENCES books(book_id)
	       ON DELETE CASCADE
	       ON UPDATE CASCADE,
       FOREIGN KEY (user_id)
       	       REFERENCES users(user_id)
	       ON DELETE CASCADE
	       ON UPDATE CASCADE
) engine=innodb ;


CREATE TABLE favorites (
       user_id int unsigned not null,
       book_id int unsigned not null,       
       primary key (book_id, user_id),
       FOREIGN KEY (book_id)
       	       REFERENCES books(book_id)
	       ON DELETE CASCADE
	       ON UPDATE CASCADE,
       FOREIGN KEY (user_id)
       	       REFERENCES users(user_id)
	       ON DELETE CASCADE
	       ON UPDATE CASCADE
       
) engine=innodb ;

-- select "hello";
-- SELECT LOAD_FILE("/var/www/html/books/models/test");
-- SELECT LOAD_FILE("/var/www/html/books/models/no_avatar.gif");


-- INSERT INTO avatars VALUES(1, LOAD_FILE("/no_avatar.gif")); 


INSERT INTO users VALUES(1, "vuongbv", "starfish", true);
INSERT INTO users VALUES(2, "rms", "null", false);
INSERT INTO users VALUES(3, "mccarthy", "cat", false);
INSERT INTO users VALUES(4, "linus", "evil", false);


INSERT INTO books VALUES(1, "The art of programming", "Donald Knuth");
INSERT INTO books VALUES(2, "Structural interpretation of Computer programs", "Gerald Sussman");
INSERT INTO books VALUES(3, "The C programming language", "Dennis Ritchie");
INSERT INTO books VALUES(4, "AI, a modern approach", "Peter Novig");

INSERT INTO comments VALUES(1, 1, 1, "A mustread book to master algorithm");
INSERT INTO comments VALUES(2, 1, 4, "I kept it under my pillow");

INSERT INTO comments VALUES(3, 2, 1, "To know what porgramming really is, read this book");
INSERT INTO comments VALUES(4, 2, 3, "Much of stuff in this book I have never dreamt before");

INSERT INTO comments VALUES(5, 3, 4, "This tell me the way to write Linux");
INSERT INTO comments VALUES(6, 3, 3, "Too different from my LISP");

INSERT INTO comments VALUES(7, 4, 1, "A approach to reach the future of computing");
INSERT INTO comments VALUES(8, 4, 2, "I have no idea why it is useful right now");

INSERT INTO favorites VALUES(1, 2);
INSERT INTO favorites VALUES(1, 4);

INSERT INTO favorites VALUES(2, 1);
INSERT INTO favorites VALUES(2, 2);
INSERT INTO favorites VALUES(2, 3);

INSERT INTO favorites VALUES(3, 1);
INSERT INTO favorites VALUES(3, 2);

INSERT INTO favorites VALUES(4,1);
INSERT INTO favorites VALUES(4,3);

