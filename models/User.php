<?php
require_once(__DIR__."/Model.php");
require_once(__DIR__."/Favorite.php");

class User extends Model {
  static $table_name = "users";
  static $primary_keys = array("user_id");
  static $field_names = array("user_id", "username", "password", "is_admin");

  function is_admin() {
    return $this->get_field("is_admin");
  }
  
  function get_favorite_books() {
    $favorite = new Favorite();
    $favorite->set_field("user_id", $_SESSION["current_user"]->get_field("user_id"));
    $favorites = $favorite->retrieve_on_fields(array("user_id"));

    $books = array();
    foreach ($favorites as $favor) {
      $book = new Book();
      $book->set_field("book_id", $favor->get_field("book_id"));
      $book->retrieve_on_key();
      
      $books[] = $book;
    }
    return $books;
  }
}
?>