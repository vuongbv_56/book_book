<?php
require_once(__DIR__."/../config.php");

class Model {
  static $table_name;
  static $field_names;
  static $primary_keys;
  
  protected $record;

  static $hostname = MYSQL_HOSTNAME;
  static $username = MYSQL_USERNAME;
  static $password = MYSQL_PASSWORD;
  static $database = MYSQL_DATABASE;

  function Model() {
    foreach (static::$field_names as $name)
      $this->record[$name] = '';
  }

  static function enquote($string) {
    return "'".mysql_real_escape_string($string)."'";
  }

  static function key_and_value($array, $keys=NULL) {
    if (is_null($keys))
      $keys = array_keys($array);

    $result = array();
    foreach ($keys as $key) {
      $result[] = $key. "=". static::enquote($array[$key]);
    }
    return $result;
  }

  static function filter_danger($field_name, $value) {
    return $value;
  }

  function connect_to_database() {
    mysql_pconnect(static::$hostname, static::$username, static::$password) or die("Connect to Mysql failed");
    mysql_select_db(static::$database);
  }

  function insert() {
    static::connect_to_database();    
    $csv_field_names = implode(",", static::$field_names);

    foreach ($this->record as $field_name => $value)
      $this->record[$field_name] = static::filter_danger($field_name, $value);
    
    $csv_values = implode(",", array_map("static::enquote", $this->record));

    $query = sprintf("INSERT INTO %s(%s) VALUES (%s)",
		     static::$table_name, $csv_field_names, $csv_values);

    return mysql_query($query);
  }

  function exist() {
    return retrieve_on_key() != NULL;
  }

  function delete() {
    static::connect_to_database();
    $conditions = implode(" and ", static::key_and_value($this->record, static::$primary_keys));
    $query = sprintf("DELETE FROM %s WHERE %s",
		     static::$table_name, $conditions);
    return mysql_query($query);
  }
  
  function retrieve_on_fields($fields) {
    static::connect_to_database();
    $instances =  static::select_on_condition(implode(" and ", static::key_and_value($this->record, $fields)));
    return $instances;
  }
  
  function retrieve_on_key() {
    $instances = $this->retrieve_on_fields(static::$primary_keys);
    if (count($instances) == 0)
      return NULL;
    else {
      $object = $instances[0];
      $this->update_from_array($object->get_information());
      return $object;
    }
  }
  


  function get_information() {
    return $this->record;
  }

  function get_field($field_name) {
    return $this->record[$field_name];
  }

  function set_field($field_name, $value) {
    if (in_array($field_name, static::$field_names))
      $this->record[$field_name] = $value;
  }

  static function select_on_condition($condition) {
    static::connect_to_database();
    $objects = array();
    
    $query = implode(" ", array("SELECT * FROM", static::$table_name, "WHERE", $condition));

    $result = mysql_query($query);

    if (!$result)
      return $objects;
    
    while ($row = mysql_fetch_assoc($result)) {
      $class_name = get_called_class();
      $object = new $class_name();
      $object->update_from_array($row);
      $objects[] = $object;
    }

    return $objects;
  }

  static function get_all() {
    return static::select_on_condition("1=1");
  }
      
    
  function update() {
    static::connect_to_database();
    $conditions = implode(" and ", static::key_and_value($this->record, static::$primary_keys));
    $settings = implode(",", static::key_and_value($this->record));
    $query = implode(" ", array("UPDATE", static::$table_name, "SET", $settings, "WHERE", $conditions));

    return mysql_query($query);
  }  
  
  function update_from_array($array, $field_names = NULL) {
    if (! $field_names)
      $field_names = static::$field_names;
    
    foreach ($array as $field => $value) {
      if (in_array($field, $field_names))
	$this->record[$field] = $array[$field];
    }
  }
}

?>