<?php
require_once(__DIR__."/Model.php");
require_once(__DIR__."/User.php");

class Comment extends Model {
  static $table_name = "comments";
  static $primary_keys = array("comment_id");
  static $field_names = array("comment_id", "book_id", "user_id", "content");

  function get_user() {
    $user = new User();
    $user->set_field("user_id", $this->get_field("user_id"));
    $user->retrieve_on_key();
    return $user;
  }

  static function filter_danger($field_names, $value) {
    if ($field_names == "content")
      return htmlspecialchars($value);
    else
      return $value;
  }
}

?>