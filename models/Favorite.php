<?php
require_once(__DIR__."/Model.php");

class Favorite extends Model {
  static $table_name = "favorites";
  static $primary_keys = array("user_id", "book_id");
  static $field_names = array("user_id", "book_id");
}
?>