<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../models/Favorite.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function favorite_delete($params) {
  require_keys(array("book_id"), $params);
  check_for_login();
  
  $new_favorite = new Favorite();
  $new_favorite->set_field("user_id", $_SESSION["current_user"]->get_field("user_id"));
  $new_favorite->set_field("book_id", $params["book_id"]);

  $result = $new_favorite->delete();
  
  $form_params["result"] = $result;
  $form = View::fetch(__DIR__."/../views/favorite_delete.tpl.php", $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Remove favorite";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

favorite_delete($_GET);

?>