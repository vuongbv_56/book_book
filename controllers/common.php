<?php
define ("NO_USER", "-1");
session_start();
require_once(__DIR__."/../config.php");

function require_keys($param_names, $param_assoc) {
  foreach ($param_names as $name) {
    if (! array_key_exists($name, $param_assoc)) {
      echo "Missing params";
      exit();
    }
   }
}

function check_for_login() {
  if (!isset($_SESSION["current_user"])) {
    $link = URL_BASE. "/login.php";
    echo "Please <a href='$link'>log in</a>.";
    exit();
  }
}

function check_for_admin() {
  check_for_login();
  check_for_privilege(NO_USER);
}
function check_for_privilege($id) {
  check_for_login();
  if (! (is_admin() or is_user($id))) {
      $link = URL_BASE."login.php";
      echo "This action is not allowed to you! Please <a href='$link'>log in</a> as an appropriate user.";
      exit();
  }
}

function is_admin() {  
  return isset($_SESSION["current_user"]) and $_SESSION["current_user"]->get_field("is_admin");
}  

function is_user($id) {
  return isset($_SESSION["current_user"]) and $_SESSION["current_user"]->get_field("user_id") == $id;
}
?>