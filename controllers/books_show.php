<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");


function books_show() {
  check_for_login();
  $books = Book::get_all();

  $books_params["books"] = $books;
  $books_views = View::fetch(PATH_BASE. "/views/books_show.tpl.php", $books_params);
  
  $layout_params["head"] = "Books show";
  $layout_params["body"] = $books_views;

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

books_show();
?>