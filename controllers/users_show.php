<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");


function users_show() {
  check_for_admin();
  
  $users = User::get_all();

  $users_params["users"] = $users;
  $users_views = View::fetch(PATH_BASE. "/views/users_show.tpl.php", $users_params);
  
  $layout_params["head"] = "Users show";
  $layout_params["body"] = $users_views;

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

users_show();
?>