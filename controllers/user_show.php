<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function user_show($params) {
  require_keys(array("user_id"), $params);
  check_for_privilege($params["user_id"]);
  $user = new User();

  $user->update_from_array($params);
  $user->retrieve_on_key();

  $view_params["user"] = $user;
  $view = View::fetch(__DIR__."/../views/user_show.tpl.php", $view_params);

  $layout_params["body"] = $view;
  $layout_params["head"] = "Show user";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

user_show($_GET);
?>