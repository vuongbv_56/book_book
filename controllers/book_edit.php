<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function book_edit($params) {
  require_keys(array("book_id"), $params);
  check_for_admin();
  $book = new Book();
  $book->update_from_array($params);
  $book->retrieve_on_key();

  $form_params["book"] = $book;
  $form_params["operation"] = "edit";
  $form = View::fetch(realpath( "../views/book_edit.tpl.php"), $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Edit book";

  View::dump(realpath( "../views/layout.tpl.php"), $layout_params);
}

book_edit($_GET);
?>