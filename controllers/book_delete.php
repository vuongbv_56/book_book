<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function book_delete($params) {
  require_keys(array("book_id"), $params);
  check_for_admin();
  
  $book = new Book();

  $book->update_from_array($params);
  $book->retrieve_on_key();
  
  $form_params["book"] = $book;
  $form = View::fetch("../views/book_delete.tpl.php", $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Delete book";

  View::dump("../views/layout.tpl.php", $layout_params);
}

book_delete($_GET);
?>