<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function book_add($params) {  
  check_for_admin();
  
  $new_book = new Book();

  $form_params["book"] = $new_book;
  $form_params["operation"] = "add";
  $form = View::fetch(__DIR__."/../views/book_edit.tpl.php", $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Add book";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

book_add($_GET);
?>