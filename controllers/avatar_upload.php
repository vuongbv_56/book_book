<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../models/Avatar.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

define ("MAX_SIZE", 20000);
function avatar_upload($params) {
  require_keys(array("user_id"), $params);
  check_for_privilege($params["user_id"]);

  if ($_FILES["upload_image"]["size"] > MAX_SIZE) {
    $error = "File must not larger than 20 Kb";
  } else {
    $info = getimagesize($_FILES["upload_image"]["tmp_name"]);
    if ($info) {
      $content = file_get_contents($_FILES["upload_image"]["tmp_name"]);
      echo strlen($content);
      $avatar = new Avatar();
      $avatar->set_field("user_id", $params["user_id"]);
      $action = $avatar->retrieve_on_key()? "update": "insert";
      $avatar->set_field("image", $content);
    
      if (! call_user_func(array($avatar,$action)))
	$error = "Fail to update the database";
    } else {
      $error = "Not an image file";
    }
  }
  if (isset($error))
    echo $error;
  else
    header("Location: ". URL_BASE. "/controllers/user_show.php?user_id=". $params["user_id"]);
}

avatar_upload($_POST);
?>
