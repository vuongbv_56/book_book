<?php
require_once(__DIR__."/../config.php");
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../models/Avatar.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function user_avatar($params) {
  require_keys(array("user_id"), $params);
  check_for_privilege($params["user_id"]);

  $avatar = new Avatar();
  $avatar->set_field("user_id", $params["user_id"]);
  $avatar->retrieve_on_key();

  
  $image = $avatar->get_field("image");
  if ($image) {
    header('Content-type: image');
    echo $image;
  } else {
    header("Location: ". URL_BASE."/models/no_avatar.gif");
  }
  
}

user_avatar($_GET);
?>