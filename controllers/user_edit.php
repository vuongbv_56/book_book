<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function user_edit($params) {
  require_keys(array("user_id"), $params);
  check_for_privilege($params["user_id"]);
  
  $user = new User();
  $user->update_from_array($params);
  $user->retrieve_on_key();

  $form_params["user"] = $user;
  $form_params["operation"] = "edit";
  $form = View::fetch(realpath( "../views/user_edit.tpl.php"), $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Edit user";

  View::dump(realpath( "../views/layout.tpl.php"), $layout_params);
}

user_edit($_GET);
?>