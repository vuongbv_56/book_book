<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/../controllers/common.php");

function book_show($params) {
  require_keys(array("book_id"), $params);
  check_for_login();
  
  $book = new Book();

  $book->update_from_array($params);
  $book->retrieve_on_key();
  
  $view_params["book"] = $book;
  $view_params["is_liked"] = $book->in_favorites_of($_SESSION["current_user"]);
  
  $view = View::fetch(__DIR__."/../views/book_show.tpl.php", $view_params);

  $layout_params["body"] = $view;
  $layout_params["head"] = "Show book";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

book_show($_GET);
?>