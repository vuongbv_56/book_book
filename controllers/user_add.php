<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/common.php");
require_once(__DIR__."/../views/View.php");

function user_add($params) {
  check_for_admin();
  
  $new_user = new User();

  $form_params["user"] = $new_user;
  $form_params["operation"] = "add";
  $form = View::fetch(__DIR__."/../views/user_edit.tpl.php", $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Add user";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

user_add($_GET);
?>