<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function book_operation($params) {
  check_for_admin();
  
  $book = new Book();
  require_keys(array("operation"), $params);

  switch ($params["operation"]) {
  case "add":    
    $updated_fields = array("title", "author");
    require_keys($updated_fields, $params);
    $book->update_from_array($params, $updated_fields);

    $result = $book->insert();
    break;
  case "edit":
    $updated_fields = array("book_id", "title", "author");
    require_keys($updated_fields, $params);
    $book->update_from_array($params, $updated_fields);
    $result = $book->update();
    break;
  case "delete":
    $updated_fields = array("book_id");
    require_keys($updated_fields, $params);
    $book->update_from_array($params, $updated_fields);

    $result = $book->delete();
    break;
  default:
    echo "Invalid operation";
    exit();
  }
  
  $form_params["result"] = $result;
  $form = View::fetch(realpath( "../views/operation_result.tpl.php"), $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Book ${params['operation']}";

  View::dump(realpath( "../views/layout.tpl.php"), $layout_params);

  /* header("Location: ". URL_BASE. "/controllers/books_show.php"); */
}

book_operation($_POST);
?>