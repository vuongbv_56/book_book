<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../models/Favorite.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function favorites_show($params) {
  check_for_login();
  
  $books = $_SESSION["current_user"]->get_favorite_books();
  $form_params["books"] = $books;
  $form = View::fetch(__DIR__."/../views/favorites_show.tpl.php", $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Favorite books";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

favorites_show($_GET);

?>