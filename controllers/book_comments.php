<?php
require_once(__DIR__."/../models/Book.php");
require_once(__DIR__."/../models/Comment.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/../controllers/common.php");

function book_comments($params) {
  require_keys(array("book_id"), $params);
  check_for_login();
  
  $book = new Book();

  $book->update_from_array($params);
  $book->retrieve_on_key();

  $view_params["comments"] = $book->get_comments();
  $view_params["book"] = $book;
  
  $view = View::fetch(__DIR__."/../views/book_comments.tpl.php", $view_params);

  $layout_params["body"] = $view;
  $layout_params["head"] = "Comments on the book";

  View::dump(__DIR__."/../views/layout.tpl.php", $layout_params);
}

book_comments($_GET);
?>