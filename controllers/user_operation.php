<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/../models/Avatar.php");
require_once(__DIR__."/common.php");

function user_operation($params) {
  check_for_login();
  
  $user = new User();
  require_keys(array("operation"), $params);
  
  $params["is_admin"] = array_key_exists("is_admin", $params);
  switch ($params["operation"]) {
  case "add":
    require_keys(array("username", "password", "is_admin"), $params);
    check_for_admin();    
    $user->update_from_array($params,
			     array("username", "password", "is_admin"));  
    $result = $user->insert();
    break;
    
  case "edit":
    $updated_fields = $_SESSION["current_user"]->is_admin()?
      array("user_id", "username", "password", "is_admin"):
      array("user_id", "username", "password");
    
    require_keys($updated_fields, $params);
    check_for_privilege($params["user_id"]);
    
    $user->update_from_array($params, $updated_fields);      
    $result = $user->update();
    break;
    
  case "delete":
    require_keys(array("user_id"), $params);
    check_for_admin();
    $user->update_from_array($params, array("user_id"));
    $result = $user->delete();
    break;
  default:
    echo "Invalid operation";
    exit();
  }

  $form_params["result"] = $result;
  $form = View::fetch(realpath( "../views/operation_result.tpl.php"), $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "User ${params['operation']}";

  View::dump(realpath( "../views/layout.tpl.php"), $layout_params);

  /* header("Location: ". URL_BASE. "/controllers/users_show.php"); */
}

user_operation($_POST);
?>