<?php
require_once(__DIR__."/../models/User.php");
require_once(__DIR__."/../models/Comment.php");
require_once(__DIR__."/../views/View.php");
require_once(__DIR__."/common.php");

function comment_add($params) {
  require_keys(array("book_id", "content"), $params);
  check_for_login();
  
  $comment = new Comment();
  $comment->update_from_array($params);
  $comment->set_field("user_id", $_SESSION["current_user"]->get_field("user_id"));
  $result = $comment->insert();
  
  $form_params["result"] = $result;
  $form = View::fetch(realpath( "../views/comment_add.tpl.php"), $form_params);

  $layout_params["body"] = $form;
  $layout_params["head"] = "Add comment result";

  View::dump(realpath( "../views/layout.tpl.php"), $layout_params);
}

comment_add($_POST);
?>