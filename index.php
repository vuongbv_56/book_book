<?php
require_once(__DIR__."/models/User.php");
require_once(__DIR__."/config.php");

session_start();

if (isset($_SESSION["current_user"]))
  header("Location: ". URL_BASE. "/controllers/user_show.php?user_id=". $_SESSION["current_user"]->get_field("user_id"));
else
  header("Location: ". URL_BASE. "/login.php");

?>